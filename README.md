# ТЕСТ 1

Необходимо написать php-демон. Со следующими функционалом:
Через cURL необходимо обратиться сюда https://syn.su/testwork.php , 
со следующими параметрами (method=get) методом POST 
2. Из JSON ответа необходимо каждый час отправлять сюда https://syn.su/testwork.php cURL`ом , 
со следующими параметрами (method=UPDATE&message=) методом POST 
значения параметра message взять из параметра message response 
зашифрованного методом XOR, 
к результату шифрования применить base64_encode. 
В случае успешного запроса в качестве результата придет JSON 
```json
{
    "errorCode": null,
    "response": "Success"
}
```

Если произошла ошибка, необходимо остановить выполнения демона 
и отправить сообщение на почту (почта для ошибок должна быть указана в качестве константы в демоне).

## Responses

```json
    {
        "response": {
            "message": "synergy.ru",
            "key": "JasdjiYlaq3"
        }
    }
```

```json
    {
     "errorCode": 15,
     "errorMessage": "Fail Method",
     "response": null
    }
```

```json
    {
        "errorCode": 15,
        "errorMessage": "Fail Method",
        "response": null
    }
```