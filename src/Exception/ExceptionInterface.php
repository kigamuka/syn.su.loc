<?php

namespace Main\Exception;

/**
 * Marker interface for all exceptions thrown by project
 * Interface ExceptionInterface
 * @package Main\Exception
 */
interface ExceptionInterface
{
}
