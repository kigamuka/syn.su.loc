<?php

namespace Main\Exception\Spl;

/**
 * Invalid Argument Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements \Main\Exception\ExceptionInterface
{
}
