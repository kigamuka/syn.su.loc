<?php

namespace Main\Exception\Exception\Spl;

/**
 * Logic exception
 */
class LogicException extends \LogicException implements \Main\Exception\ExceptionInterface
{
}
