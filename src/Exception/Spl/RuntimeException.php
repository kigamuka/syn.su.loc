<?php

namespace Main\Exception\Spl;
use Main\Exception\Marker\Base\DataLayerException;

/**
 * Runtime exception
 */
class RuntimeException extends \RuntimeException implements DataLayerException
{
}
