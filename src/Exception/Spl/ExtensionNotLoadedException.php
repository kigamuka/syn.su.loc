<?php

namespace Main\Exception\Spl;

/**
 * Extension not loaded exception
 */
class ExtensionNotLoadedException extends RuntimeException
{
}
