<?php

namespace Main\Exception\Spl;
use Main\Exception\Marker\Base\ControllerLayerException;

/**
 * Bad method call exception
 */
class BadMethodCallException extends \BadMethodCallException implements ControllerLayerException
{
}
