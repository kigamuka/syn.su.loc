<?php

namespace Main\Exception\Spl;

/**
 * Domain exception
 */
class DomainException extends \DomainException implements \Main\Exception\ExceptionInterface
{
}
