<?php

namespace Main\Exception\Spl;

/**
 * Invalid callback exception
 */
class InvalidCallbackE1xception extends DomainException implements \Main\Exception\ExceptionInterface
{
}
