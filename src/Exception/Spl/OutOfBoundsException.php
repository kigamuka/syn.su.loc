<?php

namespace Main\Exception\Spl;

use Main\Exception\ExceptionInterface;

/**
 * OutOfBounds exception
 */
class OutOfBoundsException extends \OutOfBoundsException implements ExceptionInterface
{
}
