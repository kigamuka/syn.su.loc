<?php

namespace Main\Exception;

use Main\Exception\Marker\Base\ApplicationException;

class ExtensionNotInstalledException extends DaemonException implements ApplicationException
{
}