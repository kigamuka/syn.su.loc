<?php

/**
 * @author Vitaliy Nesterko <kigamuka@gmail.com>
 */

namespace Main\Exception\Marker\Functional;

use Main\Exception\Marker;

interface PublicValidationException extends BrokenBusinessLogicException
{
    public function getInformation();
}
