<?php
/**
 * @author Vitaliy Nesterko <kigamuka@gmail.com>
 */
namespace Main\Exception\Marker\Base;

interface ApplicationException extends \Main\Exception\ExceptionInterface
{
}
