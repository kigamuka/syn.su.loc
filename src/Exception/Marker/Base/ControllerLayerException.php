<?php

/**
 * @author Vitaliy Nesterko <kigamuka@gmail.com>
 */
namespace Main\Exception\Marker\Base;

interface ControllerLayerException extends ApplicationException
{
}
