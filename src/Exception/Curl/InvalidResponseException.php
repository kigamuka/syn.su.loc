<?php

namespace Main\Exception\Curl;

use Main\Exception\CurlException;
use Main\Exception\Marker\Base\ServiceLayerException;
use Main\Exception\Marker\Functional\BrokenBusinessLogicException;

class InvalidResponseException extends CurlException implements ServiceLayerException, BrokenBusinessLogicException
{
}