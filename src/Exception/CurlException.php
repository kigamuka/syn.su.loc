<?php

namespace Main\Exception;

use Main\Exception\Marker\Base\ServiceLayerException;

class CurlException extends DaemonException implements ServiceLayerException
{
}