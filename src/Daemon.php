<?php

namespace Main;

/**
 * Class Daemon
 * @package Main
 */
class Daemon
{
    /**
     *
     */
    const EMAIL_ADDRESS = 'kigamuka@gmail.com';
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Encrypt
     */
    private $encrypt;

    /**
     * Daemon constructor.
     */
    public function __construct()
    {
        $this->request = new Request();
        $this->encrypt = new Encrypt();
    }

    /**
     * @param $code
     * @param $msg
     * @param null $var
     */
    public static function log($code, $msg, $var = null)
    {

        static $codeMap = array(
            E_ERROR   => "Error",
            E_WARNING => "Warning",
            E_NOTICE  => "Notice",
        );

        $msg = date('[d-M-Y H:i:s] ') . $codeMap[$code] . ': ' . $msg;

        if (null !== $var) {

            $msg .= "\n";
            $msg .= var_export($var, true);
            $msg .= "\n";
            $msg .= "\n";
        }
        file_put_contents(DAEMON_LOG, $msg . "\n", FILE_APPEND);
    }

    /**
     * @throws Exception\CurlException
     * @throws Exception\DaemonException
     */
    public function step()
    {
        $result = $this->request->makePost('method=get');
        $this->encrypt
            ->setKey($result['response']['key'])
            ->setMessage($result['response']['message']);
        $newMessage = $this->encrypt
            ->xor()
            ->base64()
            ->getResult();

        return $this->next($newMessage);
    }

    /**
     * @param $newMessage
     * @return bool
     * @throws Exception\CurlException
     * @throws Exception\DaemonException
     */
    public function next($newMessage)
    {
        $params = 'method=update&message=' . $newMessage;
        $result = $this->request->makePost($params);

        if ($this->isSuccessResponse($result)) {
            return true;
        }

        return false;
    }

    /**
     * @param $result
     * @return bool
     */
    private function isSuccessResponse($result)
    {
        return isset($result['response']) && 'Success' === $result['response'];
    }
}
