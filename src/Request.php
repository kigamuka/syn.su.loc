<?php

namespace Main;

use Main\Exception\CurlException;
use Main\Exception\Curl\InvalidResponseException;
use Main\Exception\Curl\StatusCodeIsNotOkException;

/**
 * Class Request
 * @package Main
 */
class Request
{
    /**
     * @var string
     */
    private $url = 'https://syn.su/testwork.php';

    /**
     * @param null $dataFields
     * @return bool|string
     * @throws CurlException
     * @throws Exception\DaemonException
     */
    public function makePost($dataFields = null)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataFields);
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_CONNECTTIMEOUT => 30,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_HTTPHEADER     => array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "cache-control: no-cache",
                ),
            )
        );

        try {
            $response   = curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $this->guardAgainstInvalidStatusCode($statusCode, $curl);

            $jsonResult = json_decode($response, true);

            $this->guardAgainstInvalidResponse($jsonResult, $response);

            curl_close($curl);

            if (null !== $jsonResult) {
                return $jsonResult;
            }
        } catch (\Main\Exception\DaemonException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new CurlException('cURL Error #:', $e->getCode(), $e);
        }

        return $response;
    }

    /**
     * @param $jsonResult
     * @param $response
     * @throws InvalidResponseException
     */
    private function guardAgainstInvalidResponse($jsonResult, $response)
    {
        if (array_key_exists('errorCode', $jsonResult) && $jsonResult['errorCode']) {
            throw new InvalidResponseException("cURL response invalid Error #:" . $response);
        }
    }

    /**
     * @param $statusCode
     * @param $curl
     * @throws StatusCodeIsNotOkException
     */
    private function guardAgainstInvalidStatusCode($statusCode, $curl): void
    {
        if (200 !== $statusCode) {
            $err = curl_error($curl);
            throw new StatusCodeIsNotOkException("cURL Error #:" . $err);
        }
    }
}