<?php

namespace Main;

/**
 * Class Encrypt
 * @package Main
 */
class Encrypt
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $message;
    /**
     * @var
     */
    private $result;

    /**
     * Encrypt constructor.
     * @param string $message
     * @param string $key
     */
    public function __construct($message = '', $key = '')
    {
        $this->message = $message;
        $this->key     = $key;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }

    /**
     * @return $this
     */
    public function xor()
    {
        $text    = $this->message;
        $key     = $this->key;
        $outText = '';

        for ($i = 0; $i < mb_strlen($text);) {
            for ($j = 0; ($j < mb_strlen($key) && $i < mb_strlen($text)); $j++, $i++) {
                $outText .= $text{$i} ^ $key{$j};
                //echo 'i=' . $i . ', ' . 'j=' . $j . ', ' . $outText{$i} . '<br />'; // For debugging
            }
        }

        $this->result = $outText;

        return $this;
    }

    /**
     * @return $this
     */
    public function base64()
    {
        $this->result = base64_encode($this->result);

        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }
}