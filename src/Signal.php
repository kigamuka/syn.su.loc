<?php

namespace Main;

/**
 * Class Signal
 * @package Main
 */
class Signal
{
    /**
     * Signal constructor.
     */
    private function __construct()
    {
    }

    /**
     * Call this method to get singleton
     *
     * @return Signal
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new Signal();
        }

        return $inst;
    }

    public function register()
    {
        declare(ticks=1);

        pcntl_signal(SIGTERM, [$this, "handler"]);
        pcntl_signal(SIGINT, [$this, "handler"]);
    }

    /**
     * @param $signal
     */
    public function handler($signal)
    {
        echo PHP_EOL . ' got CTRL+C, finishing all operations to exit...' . PHP_EOL;

        $this->cleanup();
    }

    protected function cleanup()
    {
        echo "Shutting down" . PHP_EOL;
        exit(1);
    }
}
