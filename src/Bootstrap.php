<?php

namespace Main;

use Main\Exception\ExtensionNotInstalledException;

/**
 * Class Bootstrap
 * @package Main
 */
class Bootstrap
{
    /**
     * Bootstrap constructor.
     * @throws ExtensionNotInstalledException
     */
    public function __construct()
    {

    }

    public function initGuards()
    {
        $this->guardAgainstWeHavePcntlExt();

        return $this;
    }

    public function initSignals()
    {
        Signal::Instance()->register();

        return $this;
    }

    /**
     * @throws ExtensionNotInstalledException
     */
    private function guardAgainstWeHavePcntlExt(): void
    {
        if (!function_exists("pcntl_signal")) {
            throw new ExtensionNotInstalledException('Please install pcntl ext');
        }
    }
}
