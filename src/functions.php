<?php

if (!function_exists('log')) {
    /**
     * @param $code
     * @param $msg
     * @param null $var
     */
    function log($code, $msg, $var = null)
    {

        static $codeMap = array(
            E_ERROR   => "Error",
            E_WARNING => "Warning",
            E_NOTICE  => "Notice",
        );

        $msg = date('[d-M-Y H:i:s] ') . $codeMap[$code] . ': ' . $msg;

        if (null !== $var) {

            $msg .= "\n";
            $msg .= var_export($var, true);
            $msg .= "\n";
            $msg .= "\n";
        }
        file_put_contents(DAEMON_LOG, $msg . "\n", FILE_APPEND);
    }
}
/**
 * @param $string
 * @param $key
 * @return string
 */
function xorWithKey($string, $key)
{
    // Our plaintext/ciphertext
    $text = $string;

    // Our output text
    $outText = '';

    for ($i = 0; $i < mb_strlen($text);) {
        for ($j = 0; ($j < mb_strlen($key) && $i < mb_strlen($text)); $j++, $i++) {
            $outText .= $text{$i} ^ $key{$j};
            //echo 'i=' . $i . ', ' . 'j=' . $j . ', ' . $outText{$i} . '<br />'; // For debugging
        }
    }

    return $outText;
}