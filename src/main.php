<?php

require __DIR__ . '/../vendor/autoload.php';

try {

    declare(ticks=1);
    $bootstrap = new \Main\Bootstrap();

    $bootstrap->initGuards()
        ->initSignals();

    $daemon = new \Main\Daemon();
    $daemon->step();
} catch (\Main\Exception\DaemonException $e) {

}









$daemon = new \Main\Daemon();